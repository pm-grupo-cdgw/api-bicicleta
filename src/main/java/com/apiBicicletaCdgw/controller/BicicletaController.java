package com.apiBicicletaCdgw.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * API Aluguel Bicicletas - Projeto base para a disciplina de Programação Modular.
 *
 * Os métodos da API Aluguel Bicicleta são documentados utilizando a especificação OpenAPI (Swagger) e podem ser visualizados
 * acessando a URL do domínio + "/swagger-ui.html".
 * Acesso em servidor local: http://localhost:8080/swagger-ui.html
 */

@RestController
@RequestMapping("/")
@Api(value="Spring Boot REST API Bicicleta")
public class BicicletaController {

    @GetMapping("/")
    @ApiOperation(value="Retorna informação sobre a API")
    public String getRoot () {
        return "Endpoint raiz das bicicletas.";
    }

    @GetMapping("/bicicleta")
    @ApiOperation(value="Recupera bicicletas cadastradas")
    public String getBike (){
        return "Teste bicicleta";
    }
}
