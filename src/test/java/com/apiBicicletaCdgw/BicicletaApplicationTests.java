package com.apiBicicletaCdgw;

import com.apiBicicletaCdgw.controller.BicicletaController;
import com.apiBicicletaCdgw.service.BicicletaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BicicletaApplicationTests {

	@Autowired
	private BicicletaController bicicletaController;

	@Autowired
	private BicicletaService bicicletaService;

	@Test
	void testGetRoot() {
		assertEquals("Endpoint raiz das bicicletas.", bicicletaController.getRoot());
	}

	@Test
	void testGetBike() {
		assertEquals("Teste bicicleta", bicicletaController.getBike());
	}

	@Test
	void testGetBikeService() {
		assertEquals("Array de Bicicleta Teste", bicicletaService.getBicicleta());
	}
}
